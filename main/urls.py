from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'main'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^blog/$', views.blog, name='blog'),
    url(r'^blog/(?P<alias>[0-9A-Za-z\-]+)/$', views.blog_item, name='blog-item'),
    url(r'^about/$', views.about, name='about'),
    url(r'^contacts/$', views.contacts, name='contacts'),
    url(r'^faq/$', views.faq, name='contacts'),
    url(r'^environment/$', views.environment, name='environment'),
    #url(r'^services/$', views.services, name='services'),
    url(r'^testimonials/$', views.testimonials, name='testimonials'),
    url(r'^ajax/$', views.ajax, name='ajax'),
    url(r'^services/(?P<alias>[0-9A-Za-z\-]+)/$', views.service_item, name='service'),
    url(r'^services/(?P<service_lvl1>[0-9A-Za-z\-]+)/(?P<alias>[0-9A-Za-z\-]+)/$', views.service_item_lvl2, name='service_lvl2'),
    url(r'^portfolio/$', views.portfolio, name='portfolio'),
    url(r'^portfolio/(?P<alias>[0-9A-Za-z\-]+)/$', views.portfolio_service, name='portfolio_service'),
    url(r'^portfolio/(?P<service>[0-9A-Za-z\-]+)/(?P<alias>[0-9A-Za-z\-]+)/$',
        views.portfolio_service_item, name='portfolio_service_item'),
    url(r'^(?P<alias>[0-9A-Za-z\-]+)/$', views.textpage, name='textpage'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)