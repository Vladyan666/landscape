import requests
from django.template import loader
from django.http import HttpResponse, JsonResponse
from django.template.context_processors import csrf
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.conf import settings as django_settings
from django.core.mail import send_mail, BadHeaderError, EmailMessage, get_connection

from .models import *

# Create your views here.
def default_context(request, alias, object):
    c = {}
    c.update(csrf(request))

    data = get_object_or_404(object, alias=alias)

    main_menu = TextPage.objects.filter(main_menu=True)
    services = Service.objects.all()

    context_object = {
        'data': data,
        'c': c,
        'main_menu': main_menu,
        'services': services,
    }

    return context_object


def index(request):
    context_data = default_context(request, 'index', TextPage)

    template = loader.get_template('main.html')

    blog_items = BlogItem.objects.all()[:10]
    faqs = FAQ.objects.all()
    reviews = Review.objects.all()[:5]
    portfolio_items = Portfolio.objects.all()[:10]

    context_data.update({
        'faqs': faqs,
        'blog_items': blog_items,
        'reviews': reviews,
        'portfolio_items': portfolio_items
    })

    return HttpResponse(template.render(context_data))


def textpage(request, alias):
    context_data = default_context(request, alias, TextPage)

    template = loader.get_template('textpage.html')

    services = Service.objects.all()[:5]

    context_data.update({
        'services': services
    })

    return HttpResponse(template.render(context_data))


def blog(request):
    context_data = default_context(request, 'blog', TextPage)

    template = loader.get_template('blog.html')

    tags = Tag.objects.all()

    active_tags = []

    if request.GET.get('tag'):
        if request.META.get('HTTP_REFERER') == (
                ('https://' if request.is_secure() else 'http://') + request.get_host() + request.get_full_path()):
            blog_items = BlogItem.objects.all()[:10]
            return HttpResponseRedirect(reverse('main:blog', kwargs={}))
        active_tags = [int(request.GET.get('tag', 0))]
        blog_items = BlogItem.objects.filter(tags__pk__in=[request.GET.get('tag')])
    else:
        blog_items = BlogItem.objects.all()[:10]

    context_data.update({
        'blog_items': blog_items,
        'active_tags': active_tags,
        'tags': tags
    })

    return HttpResponse(template.render(context_data))

def blog_item(request, alias):
    context_data = default_context(request, alias, BlogItem)

    template = loader.get_template('article.html')

    context_data.update({

    })

    return HttpResponse(template.render(context_data))

def services(request):
    context_data = default_context(request, 'services', TextPage)

    template = loader.get_template('main_level.html')

    return HttpResponse(template.render(context_data))


def faq(request):
    context_data = default_context(request, 'faq', TextPage)

    template = loader.get_template('faq.html')

    context_data.update({
        'faq': FAQ.objects.all()
    })

    return HttpResponse(template.render(context_data))


def service_item(request, alias):
    context_data = default_context(request, alias, Service)

    template = loader.get_template('main_level.html')

    blog = BlogItem.objects.all()[:5]

    active_tags = []

    if request.GET.get('tag'):
        if request.META.get('HTTP_REFERER') == (
                ('https://' if request.is_secure() else 'http://') + request.get_host() + request.get_full_path()):
            portfolio_items = Portfolio.objects.all()[:10]
            return HttpResponseRedirect(reverse('main:service', kwargs={'alias': context_data['data'].alias}))
        active_tags = [int(request.GET.get('tag', 0))]
        portfolio_items = Portfolio.objects.filter(tags__pk__in=[request.GET.get('tag')], service=context_data['data'])
    else:
        portfolio_items = Portfolio.objects.filter(service=context_data['data'])[:10]


    context_data.update({
        'blog': blog,
        'portfolio_items': portfolio_items,
        'active_tags': active_tags,
    })

    return HttpResponse(template.render(context_data))


def service_item_lvl2(request, service_lvl1, alias):
    service = get_object_or_404(Service, alias=service_lvl1)

    context_data = default_context(request, alias, ServiceItem)

    template = loader.get_template('second_level.html')

    blog = BlogItem.objects.all()[:5]

    context_data.update({
        'portfolio_items': Portfolio.objects.filter(tags__pk__in=[context_data['data'].pk], service=service),
        'blog': blog,
    })

    return HttpResponse(template.render(context_data))


def about(request):
    context_data = default_context(request, 'about', TextPage)

    template = loader.get_template('about.html')

    portfolio_items = Portfolio.objects.all()[:7]

    reviews = Review.objects.all()[:5]

    context_data.update({
        'reviews': reviews,
        'portfolio_items': portfolio_items
    })

    return HttpResponse(template.render(context_data))

def testimonials(request):
    context_data = default_context(request, 'testimonials', TextPage)

    template = loader.get_template('testimonials.html')

    services = Service.objects.all()[:5]
    reviews = Review.objects.all()[:5]

    context_data.update({
        'reviews': reviews,
        'services': services

    })

    return HttpResponse(template.render(context_data))

def portfolio(request):
    context_data = default_context(request, 'portfolio', TextPage)

    template = loader.get_template('portfolio.html')

    portfolio_items = Portfolio.objects.all()[:10]

    context_data.update({
        'portfolio_items': portfolio_items
    })

    return HttpResponse(template.render(context_data))


def contacts(request):
    context_data = default_context(request, 'contacts', TextPage)

    template = loader.get_template('contacts.html')

    return HttpResponse(template.render(context_data))


def environment(request):
    context_data = default_context(request, 'environment', TextPage)

    template = loader.get_template('environment.html')

    return HttpResponse(template.render(context_data))


def portfolio_service(request, alias):
    context_data = default_context(request, alias, Service)

    template = loader.get_template('portfolio_second.html')

    tags = Tag.objects.all()

    active_tags = []

    if request.GET.get('tag'):
        if request.META.get('HTTP_REFERER') == (
                ('https://' if request.is_secure() else 'http://') + request.get_host() + request.get_full_path()):
            portfolio_items = Portfolio.objects.all()[:10]
            return HttpResponseRedirect(reverse('main:portfolio_service', kwargs={'alias': context_data['data'].alias}))
        active_tags = [int(request.GET.get('tag', 0))]
        portfolio_items = Portfolio.objects.filter(tags__pk__in=[request.GET.get('tag')], service=context_data['data'])
    else:
        portfolio_items = Portfolio.objects.filter(service=context_data['data'])[:10]

    context_data.update({
        'tags': tags,
        'active_tags': active_tags,
        'portfolio_items': portfolio_items
    })

    return HttpResponse(template.render(context_data))


def portfolio_service_item(request, service, alias):
    get_object_or_404(Service, alias=service)
    context_data = default_context(request, alias, Portfolio)

    template = loader.get_template('case_page.html')

    active_tags = []

    if request.GET.get('tag'):
        if request.META.get('HTTP_REFERER') == (
                ('https://' if request.is_secure() else 'http://') + request.get_host() + request.get_full_path()):
            portfolio_items = Portfolio.objects.filter(service=context_data['data'].service)[:10]
            return HttpResponseRedirect(reverse('main:portfolio_service_item', kwargs={'service': context_data['data'].service.alias, 'alias': context_data['data'].alias}))
        active_tags = [int(request.GET.get('tag', 0))]
        portfolio_items = Portfolio.objects.filter(service=context_data['data'].service, tags__pk__in=[request.GET.get('tag')])
    else:
        portfolio_items = Portfolio.objects.filter(service=context_data['data'].service)[:10]

    context_data.update({
        'active_tags': active_tags,
        'portfolio_items': portfolio_items,
        'blog': BlogItem.objects.all()[:10]
    })

    return HttpResponse(template.render(context_data))


def ajax(request):
    if request.is_ajax():
        if request.POST.get('type', None) == 'send_form':
            from email.mime.text import MIMEText
            from email.mime.multipart import MIMEMultipart
            _default = 'Не указано'
            connection = get_connection(
                host=django_settings.EMAIL_HOST,
                username=django_settings.EMAIL_HOST_USER,
                password=django_settings.EMAIL_HOST_PASSWORD
            )
            name = request.POST.get('name', _default)
            phone = request.POST.get('phone', _default)
            email = request.POST.get('email', _default)
            message = request.POST.get('message', _default)
            if not name or not phone:
                return HttpResponse('error')
            files = []
            messaga = u'Order info:<br>Name: {}<br>Phone: {}<br>E-mail: {}<br>Message: {}'.format(
                name, phone, email, message)
            recipients = [django_settings.EMAIL_HOST_USER_RECIP]
            tlg_message = messaga.replace('<br>', '\n')
            requests.get(f"https://api.telegram.org/bot{django_settings.TELEGRAM_BOT}/sendMessage?chat_id={django_settings.TELEGRAM_GROUP}&text={tlg_message}")
            subject = u'Order from site'
            mail = EmailMessage(subject, messaga, django_settings.EMAIL_HOST_USER, recipients,
                                connection=connection)
            mail.content_subtype = "html"
            types = {
                'png': 'image/png',
                'jpg': 'image/jpg',
                'pdf': 'application/pdf'
            }

            try:
                w = request.FILES['file']
                attach_type = types[w.name.split('.')[1]]
                mail.attach(w.name, w.read(), attach_type)
            except:
                pass

            if files:
                mail.attach_file(files[0])

            mail.send(fail_silently=False)

    return HttpResponse('sended')