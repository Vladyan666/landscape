# Generated by Django 2.2.9 on 2020-02-08 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20200208_1556'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogitem',
            name='text_under_blockquote',
            field=models.TextField(blank=True, help_text='fill in together "Image under blockquote"', null=True, verbose_name='Text under Block quote'),
        ),
        migrations.AlterField(
            model_name='blogitem',
            name='image_under_blockquote',
            field=models.ImageField(blank=True, help_text='fill in together "Text under Block quote"', null=True, upload_to='', verbose_name='Image under blockquote'),
        ),
    ]
