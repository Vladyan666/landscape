from django.contrib import admin
from main.models import *

# Register your models here.
page_fields = [
    (u"Page settings", {'fields': ['alias', 'ordering']}),
    (u"SEO information", {'fields': ['seo_h1', 'seo_title', 'seo_description', 'seo_keywords', 'content', ]})
]


# Страницы
@admin.register(TextPage)
class TextPageAdmin(admin.ModelAdmin):
    list_display = ('name', 'ordering')
    list_editable = ('ordering', )
    fieldsets = [("Main Fields", {'fields': ['name', 'main_menu']}), ] + page_fields

    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}


# Страницы
@admin.register(BlogItem)
class BlogItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'alias',)
    fieldsets = [("Main Fields", {'fields': ['name', 'image_preview', 'datetime', 'tags', 'second_title', 'blockquote', 'image_under_blockquote' , 'text_under_blockquote', 'desc_before_video', 'video']}),] + page_fields
    readonly_fields = ['datetime']
    filter_horizontal = ('tags',)

    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'alias', 'ordering')
    list_editable = ('ordering',)
    fieldsets = [("Main Fields", {'fields': ['name', 'image_preview', 'image_for_slider']}),] + page_fields

    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}


@admin.register(FAQ)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name', )
    fieldsets = [("Main Fields", {'fields': ['name', 'text']}), ]


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name',)
    fieldsets = [("Main Fields", {'fields': ['name', 'ordering']})]

@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ('author', 'rating')
    fieldsets = [("Main Fields", {'fields': ['author', 'rating', 'text', 'active', 'datetime']})]


class PortfolioImageInline(admin.TabularInline):
    model = PortfolioImage
    extra = 1


@admin.register(Portfolio)
class PortfolioAdmin(admin.ModelAdmin):
    list_display = ('name', 'ordering')
    inlines = [PortfolioImageInline]
    fieldsets = [("Main Fields", {'fields': ['name', 'image_preview', 'service', 'tags', 'datetime']}), ] + page_fields
    readonly_fields = ['datetime']
    filter_horizontal = ('tags',)


    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}


@admin.register(ServiceItem)
class ServiceItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'alias', 'service', 'ordering')
    list_editable = ('ordering', )
    fieldsets = [("Main Fields", {'fields': ['name', 'image_preview', 'service']}),] + page_fields

    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}
