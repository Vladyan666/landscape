from django.db import models
from ckeditor.fields import RichTextField

class Page(models.Model):
    class Meta:
        abstract = True

    alias = models.SlugField(max_length=200, verbose_name=u"URL")
    seo_h1 = models.CharField(max_length=200, verbose_name="H1", null=True, blank=True)
    seo_title = models.CharField(max_length=200, verbose_name="Title", null=True, blank=True)
    seo_description = models.CharField(max_length=500, verbose_name="Description", null=True, blank=True)
    seo_keywords = models.CharField(max_length=500, verbose_name="Keywords", null=True, blank=True)
    menutitle = models.CharField(max_length=200, verbose_name=u"menutitle", null=True, blank=True)
    content = RichTextField(config_name='default', null=True, blank=True)


class TextPage(Page):
    class Meta:
        verbose_name = 'Text page'
        verbose_name_plural = 'Text pages'
        ordering = ['ordering']

    name = models.CharField('Name', max_length=160, unique=True)
    ordering = models.IntegerField('Ordering', default=0)
    main_menu = models.BooleanField('To main menu', default=False)

    def __str__(self):
        return self.name


class Tag(models.Model):
    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'
        ordering = ['ordering']

    name = models.CharField('Name', max_length=40)
    ordering = models.IntegerField('Ordering', default=0)

    def __str__(self):
        return self.name


class BlogItem(Page):
    class Meta:
        verbose_name = 'Blog item'
        verbose_name_plural = 'Blog items'
        ordering = ['ordering']

    name = models.CharField('Name', max_length=160)
    image_preview = models.FileField('Image Preview')
    datetime = models.DateTimeField('Add date', auto_now_add=True)
    ordering = models.IntegerField('Ordering', default=0)
    tags = models.ManyToManyField(Tag, verbose_name='Tags', blank=True)
    second_title = models.CharField('Second Title', max_length=160, null=True, blank=True)
    blockquote = models.TextField('Block quote', null=True, blank=True)
    image_under_blockquote = models.ImageField('Image under blockquote', null=True, blank=True, help_text='fill in together "Text under Block quote"')
    text_under_blockquote = models.TextField('Text under Block quote', null=True, blank=True, help_text='fill in together "Image under blockquote"')
    desc_before_video = RichTextField(config_name='default', null=True, blank=True, verbose_name='text before video')
    video = models.CharField('Video', max_length=160, help_text='https://www.youtube.com/embed/Z22t2gzs5Ds', null=True, blank=True)

    def __str__(self):
        return self.name


class OurProject(Page):
    class Meta:
        verbose_name = 'Our Project'
        verbose_name_plural = 'Our Projects'
        ordering = ['ordering']

    name = models.CharField('Name', max_length=160)
    image_preview = models.FileField('Image Preview')
    datetime = models.DateTimeField('Add Date', auto_now_add=True)
    ordering = models.IntegerField('Ordering', default=0)


    def __str__(self):
        return self.name


class Service(Page):
    class Meta:
        verbose_name = 'Service'
        verbose_name_plural = 'Services'
        ordering = ['ordering']

    name = models.CharField('Name', max_length=160)
    image_preview = models.FileField('Image Preview', null=True, blank=True)
    image_for_slider = models.FileField('Image for slider', null=True, blank=True, help_text='wide image')
    ordering = models.IntegerField('Ordering', default=0)

    def __str__(self):
        return self.name


class ServiceItem(Page):
    class Meta:
        verbose_name = 'Service item'
        verbose_name_plural = 'Service items'
        ordering = ['ordering']

    name = models.CharField('Name', max_length=160)
    image_preview = models.FileField('Image Preview', null=True, blank=True)
    service = models.ForeignKey(Service, verbose_name='Main Service', related_name="sub_services", on_delete=models.SET_NULL, null=True,
                                help_text='Complete this field')
    ordering = models.IntegerField('Ordering', default=0)

    def __str__(self):
        return self.name


class PortfolioImage(models.Model):
    class Meta:
        verbose_name = 'Portfolio Image'
        verbose_name_plural = 'Portfolio Images'
        ordering = ['ordering']

    image_preview = models.FileField('Image')
    ordering = models.IntegerField('Ordering', default=0)
    portfolio = models.ForeignKey('Portfolio', models.CASCADE, verbose_name="Portfolio",
                                  related_name="portfolio_images", null=True, blank=True)

    def __str__(self):
        return str(self.pk)


class Portfolio(Page):
    class Meta:
        verbose_name = 'Portfolio'
        verbose_name_plural = 'Portfolio Item'
        ordering = ['ordering']

    name = models.CharField('Name', max_length=160)
    image_preview = models.FileField('Image Preview', null=True, blank=True)
    tags = models.ManyToManyField(Tag, verbose_name='Tags', blank=True)
    datetime = models.DateTimeField(auto_now_add=True, verbose_name="Post Date", null=True, blank=True)
    service = models.ForeignKey(Service, models.CASCADE, verbose_name='Main Service')
    tags = models.ManyToManyField(ServiceItem, verbose_name='Services', related_name="all_services", blank=True)
    ordering = models.IntegerField('Ordering', default=0)

    def __str__(self):
        return self.name


class FAQ(models.Model):
    class Meta:
        verbose_name = 'FAQ item'
        verbose_name_plural = 'FAQ items'
        ordering = ['ordering']

    name = models.CharField('Name', max_length=160)
    text = models.TextField('Text', default='')
    ordering = models.IntegerField('Ordering', default=0)

    def __str__(self):
        return self.name


class Review(models.Model):
    class Meta:
        verbose_name = 'Review'
        verbose_name_plural = 'Reviews'
        ordering = ['-datetime']

    author = models.CharField('Sender', max_length=80)
    rating = models.IntegerField('Rating', default=5)
    active = models.BooleanField('Activity', default=False)
    text = models.TextField('Review')
    datetime = models.DateTimeField('Add Date')

    def __str__(self):
        return self.author