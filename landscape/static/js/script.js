$(document).ready(function () {

    $(window).on('scroll load', function() {
        // Шапка
        if ($(this).scrollTop() > 0) {
            $('.header').addClass('active');
        } else {
            $('.header').removeClass('active');
        }

        $('.paral').each(function(index, elem) {
            var top = 0;

            if ($(window).scrollTop() > $(this).offset().top - $(window).height() / 2) {
                top = 50 - Math.floor(($(this).offset().top - $(window).scrollTop() - $(".header").outerHeight()) / 8);
                
                if ($(this).parent().hasClass("active")) {
                    if (top > 0 && top < 50) {
                        $(this).css("transform", "scale(1.6) translateY(" + top + "px)");
                    }
                }
            }
        });

        animations();
    });

    // Анимации
    function animations() {
        $(".animation").each(function () {
            var windowTop = $(window).height() * .9 + $(window).scrollTop(),
                objectTop = $(this).offset().top;

            if (windowTop > objectTop) {
                $(this).addClass('animated');
            } else {
                $(this).removeClass('animated');
            }
        });
    }

    // Слайдер на главной
    $(".main_slider__button").on("click", function() {
        var $parent = $(this).closest(".main_slider"),
            id = $parent.find(".main_slider__slide.active").index() + 2,
            length = $parent.find(".main_slider__slide").length;
  
        if (id > length) {
            id = 1;
        }
        
        $parent.find(".main_slider__slide").removeClass("active");
        $parent.find(".main_slider__slide:nth-child(" + id + ")").addClass("active");
    });

    // Slider our_main
    if ($('.our_main').length) {
        $('.our_main').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<div class="slider_arrow slider_arrow-prev">Prev.</div>',
            nextArrow: '<div class="slider_arrow slider_arrow-next">Next</div>',
            appendArrows: $('.our__navs'),
            asNavFor: '.our_second'
        });
    }

    // SLider "Real our_second"
    if ($('.our_second').length) {
        $('.our_second').slick({
            slidesToShow: 4,
            infinite: true,
            arrows: false,
            asNavFor: '.our_main'
        });
    }

    // Slider links__slider
    if ($('.links__slider').length) {
        $('.links__slider').slick({
            slidesToShow: 5,
            slidesToScroll: 5,
            prevArrow: '<div class="slider_arrow slider_arrow-prev">Prev.</div>',
            nextArrow: '<div class="slider_arrow slider_arrow-next">Next</div>',
            appendArrows: $('.links__navs'),
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                }
            ]
        });
    }

    // Slider customers__slider
    if ($('.customers__slider').length) {
        $('.customers__slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<div class="slider_arrow slider_arrow-prev">Prev.</div>',
            nextArrow: '<div class="slider_arrow slider_arrow-next">Next</div>',
            appendArrows: $('.customers__navs')
        });
    }

    // Открыть / закрыть FAQ
    $(".faq__title").on("click", function() {
        $(this).closest(".faq__el").toggleClass("active");
    });

    // Slider latest_slider
    if ($('.latest_slider').length) {
        $('.latest_slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '.latest_slider__left'
        });
    }

    // SLider "Real our_second"
    if ($('.latest_slider__left').length) {
        $('.latest_slider__left').slick({
            slidesToShow: 2,
            infinite: true,
            prevArrow: '<div class="slider_arrow slider_arrow-prev">Prev.</div>',
            nextArrow: '<div class="slider_arrow slider_arrow-next">Next</div>',
            appendArrows: $('.latest__nav'),
            asNavFor: '.latest_slider'
        });
    }

    // Close modal
    $(".overlay, .button-modal_conf").on("click", function() {
        $(".overlay, .modal").removeClass("active");
    });

    $(".modal__close").on("click", function() {
        $(this).closest(".modal").removeClass("active");
        $(".overlay").removeClass("active");

        if ($(this).hasClass("modal__close-modal_services")) {
            $(".header_drop").removeClass("hide");
            
            if ($(".header_drop").hasClass("active")) {
                $(".header_drop").removeClass("active");
            }
        }
    });

    // Open our services
    $(".open_sevices").on("click", function() {
        $(".modal_services").addClass("active");

        if ($(this).hasClass("our_services-header_drop")) {
            $(".header_drop").addClass("hide");
        } else {
            $(".overlay").addClass("active");
        }
    });

    // Open send enquery
    $(".open_enquery").on("click", function() {
        $(".modal_enquery, .overlay").addClass("active");
    });

    // Open send project
    $(".open_project").on("click", function() {
        $(".modal_project, .overlay").addClass("active");
    });

    // Open confirmation
    $(".open_conf").on("click", function() {
        $(".modal_conf, .overlay").addClass("active");
    });

    // Our services
    $(".modal_navs__nav").on("mouseenter", function() {
        var id = $(this).data("id");

        $(".modal_navs__nav, .modal_services__wrp").removeClass("active");

        $(this).addClass("active");
        $(".modal_services__wrp[data-id=" + id + "]").addClass("active");
    });

    // Open mobile menu
    $(".header_menu").on("click", function() {
        $(".header_drop").addClass("active");
    });

    // Close mobile menu
    $(".modal__close-header_drop").on("click", function() {
        $(".header_drop").removeClass("active")
        $(".header_drop").removeClass("hide");
    });

    $('.header_drop').on("click", function(e) {
        var targetbox = $('.header_drop__wrp');
        
		if (!targetbox.is(e.target) && targetbox.has(e.target).length === 0) {
            $(".header_drop, .modal_services").removeClass("active");
            $(".header_drop").removeClass("hide");
		}
    });
    
    // Close our services mobile
    $(".our_services-modal_services").on("click", function() {
        $(".modal_services").removeClass("active")
        $(".header_drop").removeClass("hide");
    });

    // To top
    $(window).on("scroll load", function() {
        if ($(this).scrollTop() > 2000) {
            $('.to_top').addClass('active');
        } else {
            $('.to_top').removeClass('active');
        }
    });

    $(".to_top").on("click", function() {
        $('body, html').animate({
            scrollTop: 0
        }, 300);
    });

    // Open / close footer menu mobile
    $(".footer__title").on("click", function() {
        $(this).closest(".footer__col").toggleClass("active")
    });

    // Page gallery img
    $(".page_case__img").on("click", function() {
        var index = $(this).index() + 1;

        $(".page_case__right .main_slider__slide").removeClass("active");
        $(".page_case__right .main_slider__slide:nth-child(" + index + ")").addClass("active");
    });

    // Open page slider
    $(".main_slider__slide").on("click", function() {
        var index = $(this).index() ;

        $(".overlay, .case_modal").addClass("active");
        $('.case_modal__slider').slick('slickGoTo', index);
    });

    // Slider case_modal__slider
    if ($('.case_modal__slider').length) {
        $('.case_modal__slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<div class="slider_arrow slider_arrow-prev">Prev.</div>',
            nextArrow: '<div class="slider_arrow slider_arrow-next">Next</div>'
        });
    }

});

// Прелоадер
$(window).bind("load", function() {
    $('.preloader').removeClass("active");
});